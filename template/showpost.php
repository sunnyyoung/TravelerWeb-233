<?php
include 'config/DB.php';
$connection = new DB();
$id = $_GET['id'];
$result = $connection->get_one("SELECT * FROM data WHERE id='" . $id . "'");
if ($result == NULL)
{
    echo '<script>alert("没有该文章!");location.href="index.php";</script>';
    exit();
} else
{
    $title = $result['title'];
    $introduction = $result['introduction'];
    $details = $result['details'];
    $time = $result['time'];
    $image = $result['image'];
}
?>
<div id="carbonForm" style="margin-top: 60px; width: 60%;">
    <h1><?php echo $title; ?></h1>
    <p>
        <label>简介</label>
        <label style="float: right"><?php echo $time; ?></label>
    </p>
    <fieldset>
        <?php echo $introduction; ?>
    </fieldset>

    <p>
        <label>详细内容</label>
    </p>
    <fieldset>
        <img src="<?php echo $image; ?>" style="width: 40%;float: right;border-radius: 6px; padding: 0px 0px 0px 5px;" />
        <?php echo $details; ?>
    </fieldset>
</div>
<div id="carbonForm" style="margin-top: 5px;margin-bottom: 35px; width: 60%;">
    <h1>评论</h1>

    <?php
    if (isset($_SESSION['username'], $_SESSION['password']))
    {
        include 'comment.php';
    } else
    {
        echo '你未<a href="?page=login" style="color: red">登录</a>,不能评论';
    }
    ?>

    <p>
        <label>网友评论</label>
    </p>

    <?php
    $comments = $connection->get_all('SELECT * FROM comment WHERE id=' . $id);
    foreach ($comments as $comment)
    {
        echo '<fieldset>';
        if (file_exists('./upload/head/' . $comment['username'] . '.jpg'))
        {
            echo '<img src="./upload/head/' . $comment['username'] . '.jpg" height="50px" style="float: left;border-radius: 6px;" />';
        } else
        {
            echo '<img src="./upload/head/default.jpg" height="50px" style="float: left;border-radius: 6px;" />';
        }
        echo $comment['time'] . '<br/>';
        echo $comment['username'] . '说: ' . $comment['details'];
        echo '</fieldset>';
    }
    ?>
</div>