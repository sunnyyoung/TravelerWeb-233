<?php
include './config/DB.php';
include './config/UPLOAD.php';
$connection = new DB();
$upload = new UPLOAD();
?>
<div id="carbonForm" style="margin-top: 60px;">
    <h1>修改头像</h1>
    <form method="post" enctype="multipart/form-data">
        <fieldset style="text-align: center;">
            <?php
            $info = $connection->get_one('SELECT * FROM user WHERE username="' . $_SESSION['username'] . '"');
            if (file_exists('./upload/head/' . $_SESSION['username'] . '.jpg'))
            {
                echo '<img src="./upload/head/' . $_SESSION['username'] . '.jpg" width="50%" />';
            } else
            {
                echo '<img src="./upload/head/default.jpg" width="50%" />';
            }
            ?>
            <br/>
            <input type="file" name="picfile" />
        </fieldset>
        <fieldset>
            <div class="formRow">
                <div class="label">
                    <label for="name">姓名:</label>
                </div>
                <div class="field">
                    <input type="text" name="realname" value="<?php echo $info['realname']; ?>"/>
                </div>
            </div>

            <div class="formRow">
                <div class="label">
                    <label for="name">电话:</label>
                </div>
                <div class="field">
                    <input type="text" name="telephone" value="<?php echo $info['telephone']; ?>"/>
                </div>
            </div>
        </fieldset>
        <div class="signupButton">
            <input type="submit" name="btnChange" id="submit">
        </div>
    </form>
</div>
<?php
if (isset($_POST['btnChange']))
{
    $realname = $_POST['realname'];
    $telephone = $_POST['telephone'];
    if ($_FILES['picfile']['name'] != '')
    {
        unlink('./upload/head/' . $_SESSION['username'] . ' . jpg');

        $upload->photp_path = './upload/head/';
        $upload->get_tmp_name($_FILES['picfile']['tmp_name']);
        $upload->get_type($_FILES['picfile']['type']);
        $upload->get_size($_FILES['picfile']['size']);
        $upload->get_name($_SESSION['username'], FALSE);
        $upload->upload_run();
    }
    $userinfo = array('realname' => $realname, 'telephone' => $telephone);

    if ($connection->update('user', $userinfo, 'username="' . $_SESSION['username'] . '"'))
    {
        echo '<script language = "javascript">alert("修改成功!");</script>';
    }
}
?>