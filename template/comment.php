<fieldset style="height: 150px;">
    <?php
    if (file_exists('./upload/head/' . $_SESSION['username'] . '.jpg'))
    {
        echo '<img src="./upload/head/' . $_SESSION['username'] . '.jpg" height="100px" style="float: left;border-radius: 6px;" />';
    } else
    {
        echo '<img src="./upload/head/default.jpg" height="100px" style="float: left;border-radius: 6px;" />';
    }
    ?>
    <fieldset style="height: 100px;">
        <form method="post">
            <textarea name="thiscomment" style="height: 90%;width: 100%;"></textarea>
            <input type="submit" name="submit" style="float: right"/>
        </form>
    </fieldset>
</fieldset>
<?php
if (isset($_POST['submit']))
{
    $thiscomment = $_POST['thiscomment'];
    if ($thiscomment == '')
    {
        echo '<script language="javascript">alert("内容不能为空,请重试!");</script>';
        return;
    }
    $commentdata = array(
        'id' => $id,
        'username' => $_SESSION['username'],
        'details' => $thiscomment,
        'time' => date('Y-m-d_H:i:s', time())
    );
    $commentresult = $connection->insert('comment', $commentdata);
    if ($commentresult)
    {
        echo '<script language="javascript">alert("评论成功!");</script>';
    } else
    {
        echo '<script language="javascript">alert("评论失败!");</script>';
    }
}
?>