<div id="carbonForm" style="margin-top: 60px;">
    <h1>注册</h1>
    <form method="post">
        <fieldset>
            <div class="formRow">
                <div class="label">
                    <label for="name">用户名:</label>
                </div>
                <div class="field">
                    <input type="text" name="username">
                </div>
            </div>

            <div class="formRow">
                <div class="label">
                    <label for="pass">密码:</label>
                </div>
                <div class="field">
                    <input type="password" name="password1">
                </div>
            </div>

            <div class="formRow">
                <div class="label">
                    <label for="pass">重复:</label>
                </div>
                <div class="field">
                    <input type="password" name="password2">
                </div>
            </div>

            <div class="formRow">
                <div class="label">
                    <label for="pass">姓名:</label>
                </div>
                <div class="field">
                    <input type="text" name="realname">
                </div>
            </div>

            <div class="formRow">
                <div class="label">
                    <label for="pass">电话:</label>
                </div>
                <div class="field">
                    <input type="text" name="telephone">
                </div>
            </div>
        </fieldset>
        <div class="signupButton">
            <input type="submit" name="btnRegist" id="submit">
        </div>
    </form>
</div>
<?php
//连接数据库初始化
include 'config/DB.php';
$connection = new DB();
if (isset($_POST['btnRegist']))
{
    if ($_POST['username'] == '' || $_POST['password1'] == '' || $_POST['password2'] == '' || $_POST['realname'] == '' || $_POST['telephone'] == '')
    {
        echo '<script language="javascript">alert("信息填写不完整!");</script>';
        return;
    }
    if ($_POST['password1'] != $_POST['password2'])
    {
        echo '<script language="javascript">alert("两次密码输入不正确,请重试!");</script>';
        return;
    } else
    {
        $password = md5($_POST['password1']);
    }
    $userinfo = array
        (
        'username' => $_POST['username'],
        'password' => $password,
        'realname' => $_POST['realname'],
        'telephone' => $_POST['telephone']
    );
    $result = $connection->insert('user', $userinfo);
    if ($result)
    {
        echo '<script language="javascript">alert("注册成功!");location.href="index.php?page=login";</script>';
    } else
    {
        echo '<script language="javascript">alert("不知道为什么注册不了啊!");</script>';
    }
}
?>