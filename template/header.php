<div class="navbg">
    <div class="navcol">
        <ul id="navul">
            <li class="navhome">
                <a href="index.php">首页</a>
            </li>
            <li>
                <a href="#">分类</a>
                <ul>
                    <li><a href="#">最新活动</a></li>
                    <li><a href="#">国内美景</a></li>
                </ul>
            </li>

            <li>
                <a href="?page="><font color="red">随便看看</font></a>
            </li>
            <li>
                <a href="?page=search">搜索</a>
            </li>
            <?php
            //判断是否登录
            if (isset($_SESSION['username'], $_SESSION['password']))
            {
                echo '<li style="float: right;">';
                echo '<a href="#">' . $_SESSION['username'] . '</a>';
                echo '<ul>';
                echo '<li><a href="?page=changeinfo">修改信息</a></li>';
                echo '<li><a href="?page=changepsw">修改密码</a></li>';
                echo '<li><a href="?page=logout">退出</a></li>';
                echo '</ul>';
                echo '</li>';
                echo '<li class="navhome" style="float: right;">';
                if (file_exists('./upload/head/' . $_SESSION['username'] . '.jpg'))
                {
                    echo '<img src="./upload/head/' . $_SESSION['username'] . '.jpg" height=40px />';
                } else
                {
                    echo '<img src="./upload/head/default.jpg" height=40px />';
                }
                echo '</li>';
            } else
            {
                echo '<li style="float: right;">';
                echo '<a href="?page=register">注册</a>';
                echo '</li>';
                echo '<li style="float: right;">';
                echo '<a href="?page=login">登录</a>';
                echo '</li>';
            }
            ?>
        </ul>
    </div>
</div>