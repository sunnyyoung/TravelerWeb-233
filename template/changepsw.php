<div id="carbonForm" style="margin-top: 60px;">
    <h1>修改密码</h1>
    <form method="post">
        <fieldset>

            <div class="formRow">
                <div class="label">
                    <label for="name">原密码:</label>
                </div>
                <div class="field">
                    <input type="password" name="password0">
                </div>
            </div>

            <div class="formRow">
                <div class="label">
                    <label for="pass">新密码:</label>
                </div>
                <div class="field">
                    <input type="password" name="password1">
                </div>
            </div>

            <div class="formRow">
                <div class="label">
                    <label for="pass">重复:</label>
                </div>
                <div class="field">
                    <input type="password" name="password2">
                </div>
            </div>
        </fieldset>
        <div class="signupButton">
            <input type="submit" name="btnChange" id="submit">
        </div>
    </form>
</div>
<?php
include 'config/DB.php';
$connection = new DB();
if (isset($_POST['btnChange']))
{
    $password0 = $_POST['password0'];   //原始密码
    $password1 = $_POST['password1'];   //新密码
    $password2 = $_POST['password2'];   //重复新密码
    if ($password0 == '' || $password1 == '' || $password2 == '')
    {
        //判断资料是否填写完整
        echo '<script language="javascript">alert("信息填写不完整!");</script>';
        return;
    } else if ($_POST['password1'] != $_POST['password2'])
    {
        //判断两次密码是否一致
        echo '<script language="javascript">alert("两次密码输入不正确,请重试!");</script>';
        return;
    } elseif (md5($password0) != $_SESSION['password'])
    {
        //判断原始密码是否正确
        echo '<script language="javascript">alert("原始密码错误,请重试!");</script>';
    } else
    {
        $userinfo = array('password' => md5($password1));
        //更新数据库密码
        $result = $connection->update('user', $userinfo, "username='" . $_SESSION['username'] . "'");
        if ($result)
        {
            session_destroy();
            echo '<script language="javascript">alert("修改密码成功!请重新登录!");location.href="index.php?page=login";</script>';
        } else
        {
            echo '<script language="javascript">alert("修改失败!");</script>';
        }
    }
}
?>