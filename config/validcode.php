<?php

session_start();
$image = imagecreate(60, 20);
imagefill($image, 0, 0, imagecolorallocate($image, 255, 255, 255));
$allSymbol = array('plus', 'minus', 'time');
$a = rand(1, 9);//随机算术参数a
$b = rand(1, 9);//随机算术参数b
switch ('time')//随机算术符号
{
    case 'plus':
        $validcode = $a + $b;
        $imgString = $a . '+' . $b . '=' . '?';
        break;
    case 'minus':
        $validcode = $a - $b;
        $imgString = $a . '-' . $b . '=' . '?';
        break;
    case 'time':
        $validcode = $a * $b;
        $imgString = $a . '*' . $b . '=' . '?';
        break;
    default:
        break;
}
//正确结果加密存放于session中
$_SESSION['validcode'] = md5($validcode);
imagestring($image, 5, 2, 2, $imgString, imagecolorallocate($image, 255, 0, 0));
header("content-type:image/png");
imagepng($image);
imagedestroy($image);
?>