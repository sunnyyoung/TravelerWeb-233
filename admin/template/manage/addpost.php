<?php
if (!isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">alert("丢!你都无登录!!!");location.href="index.php";</script>';
}
?>
<div class="content">
    <ul class="breadcrumb">
        <li class="active">新建文章</li>
    </ul>
    <div id="main">
        <br/>
        <form method="post" enctype="multipart/form-data" >
            <fieldset>
                <p>
                    <label>标题</label>
                    <input type="text" name="title" class="text-long" />
                </p>
                <p>
                    <label>简介</label>
                    <textarea name="introduction" class="text-area"></textarea>
                </p>

                <p>
                    <label>详细介绍</label>
                    <textarea name="details" class="text-area"></textarea>
                </p>

                <p>
                    <label>图片上传</label>
                    <input type="file" name="picfile" class="text-long" />
                </p>
                <input name="btnAdd" type="submit" class="btn btn-primary" id="submit" value="提交" />
            </fieldset>
        </form>
    </div>
</div>
<?php
//连接数据库初始化
include '../config/DB.php';
include '../config/UPLOAD.php';
$connection = new DB();
$upload = new UPLOAD();
if (isset($_POST['btnAdd']))
{
    $title = $_POST['title'];
    $introduction = $_POST['introduction'];
    $details = $_POST['details'];
    if ($_FILES['picfile']['name'] == '')
    {
        $upload->showerror('没有选择图片!');
    } else if ($title == '' || $introduction == '' || $details == '')
    {
        $upload->showerror('信息填写不完整!');
    }
    $upload->get_tmp_name($_FILES['picfile']['tmp_name']);
    $upload->get_type($_FILES['picfile']['type']);
    $upload->get_size($_FILES['picfile']['size']);
    $upload->get_name($_FILES['picfile']['name']);
    $upload->upload_run();

    $image = $upload->get_filepath();

    $post = array
        (
        'image' => $image,
        'title' => $title,
        'introduction' => $introduction,
        'details' => $details,
        'time' => date('Y-m-d H:i:s', time())
    );
    $result = $connection->insert('data', $post);
    if ($result)
    {
        echo '<script language="javascript">alert("添加成功!");</script>';
    } else
    {
        echo '<script language="javascript">alert("添加失败!");</script>';
    }
}
?>