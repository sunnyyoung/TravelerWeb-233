<?php
if (!isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">alert("丢!你都无登录!!!");location.href="index.php";</script>';
}
?>
<div class="content">
    <ul class="breadcrumb">
        <li class="active">所有用户</li>
    </ul>
    <div id="main">
        <br/>
        <table class="tablesorter" style="width: 100%;">
            <thead>
                <tr>
                    <th class="header">用户名</th>
                    <th style="width: 38%;" class="header">密码</th>
                    <th style="width: 20%;" class="header">真实姓名</th>
                    <th style="width: 20%;" class="header">电话号码</th>
                    <th style="width: 12%;" class="header">操作</th>
                </tr>
            </thead>
            <tbody>
                <?php
                //连接数据库初始化
                include '../config/DB.php';
                $connection = new DB();
                $scopestart = $_GET['scope'] - 10;  //范围减10
                $scopeend = $_GET['scope'];         //范围最大值
                $result = $connection->get_all('SELECT * FROM user LIMIT ' . $scopestart . ' , ' . 10);
                foreach ($result as $item)
                {
                    echo '<tr>';
                    echo '<td>' . $item['username'] . '</td>';
                    echo '<td>' . $item['password'] . '</td>';
                    echo '<td>' . $item['realname'] . '</td>';
                    echo '<td>' . $item['telephone'] . '</td>';
                    echo '<td class="action"><a href=home.php?page=modifyuser&username=' . $item['username'] . '>修改 </a><a href=home.php?page=delete&item=user&username=' . $item['username'] . '><font color="red"> 删除</font></a></td>';
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
        <div class="pagination">

            <a class="inactive" 
            <?php
            $allrows = $connection->num_rows($connection->query('SELECT * FROM user')); //获取表总行数
            if ($allrows > 10)
            {
                //当表总行数大于10的时候,计算出最大页数
                if ($allrows % 10 == 0)
                {
                    $pageend = (int) ($allrows / 10);
                } else
                {
                    $pageend = (int) (($allrows / 10) + 1);
                }
            } else
            {
                //当表总行数小于10的时候,不用算啦最大页数为1
                $pageend = 1;
            }
            //显示上一页
            if ($scopeend > 10)
            {
                //不是在第一页,上一页按钮有效
                echo 'href="?page=alluser&scope=' . ($scopeend - 10) . '"';
            } else
            {
                //在第一页,上一页按钮无效
                echo 'disabled="disabled"';
            }
            ?> >上一页
            </a>

            <?php
            //显示页数
            for ($page = 1; $page <= $pageend; $page++)
            {
                if (($page * 10) == $scopeend)
                {
                    //高亮显示当前页码
                    echo '<span class="current">' . $page . '</span>';
                } else
                {
                    //其他页码
                    echo '<a class="inactive" href="?page=alluser&scope=' . $page * 10 . '">' . $page . '</a>';
                }

                //当页码大于15的时候,无论如何还是会中断,限制页数显示
                if ($page >= 15)
                {
                    break;
                }
            }
            ?>

            <a class="inactive"
               <?php
               //显示上一页
               if (($scopeend / 10) < $pageend)
               {
                   echo 'href="?page=alluser&scope=' . ($scopeend + 10) . '"';
               } else
               {
                   echo 'disabled="disabled"';
               }
               ?> >下一页
            </a>
        </div>
    </div>
</div>