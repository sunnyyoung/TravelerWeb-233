<?php
if (!isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">alert("丢!你都无登录!!!");location.href="index.php";</script>';
}
include '../config/DB.php';
$connection = new DB();
$id = $_GET['id'];
$result = $connection->get_one("SELECT * FROM data WHERE id='" . $id . "'");
if ($result == NULL)
{
    echo '<script>alert("没有该文章!");location.href="home.php?page=allpost&scope=10";</script>';
    exit();
} else
{
    $title = $result['title'];
    $introduction = $result['introduction'];
    $details = $result['details'];
    $image = $result['image'];
}
?>
<div class="content">
    <ul class="breadcrumb">
        <li class="active">新建文章</li>
    </ul>
    <div id="main">
        <br/>
        <form method="post" enctype="multipart/form-data" >
            <fieldset>
                <p>
                    <label>标题</label>
                    <input type="text" name="title" class="text-long" value="<?php echo $title; ?>" />
                </p>
                <p>
                    <label>简介</label>
                    <textarea name="introduction" class="text-area" ><?php echo $introduction; ?></textarea>
                </p>

                <p>
                    <label>详细介绍</label>
                    <textarea name="details" class="text-area"><?php echo $details; ?></textarea>
                </p>

                <label>图片修改</label>
                <fieldset>
                    <img src="<?php echo '../' . $image; ?>" height="200px" />
                    <br/>
                    <input type="file" name="picfile" class="text-long" />
                </fieldset>

                <input name="btnChange" type="submit" class="btn btn-primary" id="submit" value="提交" />
            </fieldset>
        </form>
    </div>
</div>
<?php
include '../config/UPLOAD.php';
$upload = new UPLOAD();
if (isset($_POST['btnChange']))
{
    $title = $_POST['title'];
    $introduction = $_POST['introduction'];
    $details = $_POST['details'];
    //更换新图片啦~
    if ($_FILES['picfile']['name'] != '')
    {
        unlink('../' . $image);
        $upload->get_tmp_name($_FILES['picfile']['tmp_name']);
        $upload->get_type($_FILES['picfile']['type']);
        $upload->get_size($_FILES['picfile']['size']);
        $upload->get_name($_FILES['picfile']['name']);
        $upload->upload_run();
        $image = $upload->get_filepath();
    } else if ($title == '' || $introduction == '' || $details == '')
    {
        $upload->showerror('信息填写不完整!');
    }

    $post = array
        (
        'image' => $image,
        'title' => $title,
        'introduction' => $introduction,
        'details' => $details
    );
    $result = $connection->update('data', $post, 'id=' . $id);
    if ($result)
    {
        echo '<script language="javascript">alert("修改成功!");location.href="home.php?page=modifypost&id=' . $id . '";</script>';
    } else
    {
        echo '<script language="javascript">alert("修改失败!");</script>';
    }
}
?>