<?php
if (!isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">alert("丢!你都无登录!!!");location.href="index.php";</script>';
}
?>
<div class="content">
    <ul class="breadcrumb">
        <li class="active">所有文章</li>
    </ul>
    <div id="main">
        <br/>
        <table class="tablesorter" style="width: 100%;">
            <thead>
                <tr>
                    <th class="header">ID</th>
                    <th style="width: 30%;" class="header">标题</th>
                    <th style="width: 50%;" class="header">简介</th>
                    <th style="width: 8%;" class="header">评论</th>
                    <th style="width: 12%;" class="header">操作</th>
                </tr>
            </thead>
            <tbody>
                <?php
                //连接数据库初始化
                include '../config/DB.php';
                $connection = new DB();
                $scopestart = $_GET['scope'] - 10;  //范围减10
                $scopeend = $_GET['scope'];         //范围最大值
                $result = $connection->get_all('SELECT * FROM data LIMIT ' . $scopestart . ' , ' . 10);
                foreach ($result as $item)
                {
                    echo '<tr>';
                    echo '<td>' . $item['id'] . '</td>';
                    echo '<td><a href="../index.php?page=showpost&id=' . $item['id'] . '">' . mb_substr($item['title'], 0, 10, 'utf-8') . '</a></td>';
                    echo '<td>' . mb_substr($item['introduction'], 0, 60, 'utf-8') . '</td>';
                    echo '<td>' . $connection->num_rows($connection->query('SELECT * FROM comment WHERE id=' . $item[id])) . '</td>';
                    echo '<td class="action"><a href="home.php?page=modifypost&id=' . $item['id'] . '">修改 </a>' . '<a href=home.php?page=delete&item=post&id=' . $item['id'] . '><font color="red"> 删除</font></a></td>';
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
        <div class="pagination">

            <a class="inactive" 
            <?php
            $allrows = $connection->num_rows($connection->query('SELECT * FROM data')); //获取表总行数
            if ($allrows > 10)
            {
                //当表总行数大于10的时候,计算出最大页数
                if ($allrows % 10 == 0)
                {
                    $pageend = (int) ($allrows / 10);
                } else
                {
                    $pageend = (int) (($allrows / 10) + 1);
                }
            } else
            {
                //当表总行数小于10的时候,不用算啦最大页数为1
                $pageend = 1;
            }
            //显示上一页
            if ($scopeend > 10)
            {
                //不是在第一页,上一页按钮有效
                echo 'href="?page=allpost&scope=' . ($scopeend - 10) . '"';
            } else
            {
                //在第一页,上一页按钮无效
                echo 'disabled="disabled"';
            }
            ?> >上一页
            </a>

            <?php
            //显示页数
            for ($page = 1; $page <= $pageend; $page++)
            {
                if (($page * 10) == $scopeend)
                {
                    //高亮显示当前页码
                    echo '<span class="current">' . $page . '</span>';
                } else
                {
                    //其他页码
                    echo '<a class="inactive" href="?page=allpost&scope=' . $page * 10 . '">' . $page . '</a>';
                }

                //当页码大于20的时候,无论如何还是会中断,限制页数显示
                if ($page >= 20)
                {
                    break;
                }
            }
            ?>

            <a class="inactive"
               <?php
               //显示下一页
               if (($scopeend / 10) < $pageend)
               {
                   echo 'href="?page=allpost&scope=' . ($scopeend + 10) . '"';
               } else
               {
                   echo 'disabled="disabled"';
               }
               ?> >下一页
            </a>
        </div>
    </div>
</div>