<?php
if (!isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">alert("丢!你都无登录!!!");location.href="index.php";</script>';
}
?>
<div class="content">
    <ul class="breadcrumb">
        <li class="active">新建用户</li>
    </ul>
    <div id="main">
        <br/>
        <form method="post">
            <fieldset>
                <p>
                    <label>帐号:</label>
                    <input name="username" type="text" class="text-medium" />
                </p>

                <p>
                    <label>密码:</label>
                    <input name="password1" type="password" class="text-long" />
                </p>

                <p>
                    <label>重复密码:</label>
                    <input name="password2" type="password" class="text-long" />
                </p>

                <p>
                    <label>真实姓名:</label>
                    <input name="realname" type="text" class="text-long" />
                </p>

                <p>
                    <label>电话号码:</label>
                    <input name="telephone" type="text" class="text-long" />
                </p>
                <input name="btnAdd" type="submit" class="btn btn-primary" id="submit" value="确定" />
            </fieldset>
        </form>
    </div>
</div>
<?php
include '../config/DB.php';
$connection = new DB();
if (isset($_POST['btnAdd']))
{
    $username = $_POST['username'];
    $password1 = $_POST['password1'];   //密码
    $password2 = $_POST['password2'];   //重复密码
    $realname = $_POST['realname'];
    $telephone = $_POST['telephone'];
    if ($username == '' || $password1 == '' || $password2 == '')
    {
        //判断资料是否填写完整
        echo '<script language="javascript">alert("信息填写不完整!");</script>';
        return;
    } else if ($_POST['password1'] != $_POST['password2'])
    {
        //判断两次密码是否一致
        echo '<script language="javascript">alert("两次密码输入不正确,请重试!");</script>';
        return;
    } else
    {
        $userinfo = array
            (
            'username' => $username,
            'password' => md5($password1),
            'realname' => $realname,
            'telephone' => $telephone
        );
        $result = $connection->insert('user', $userinfo);
        if ($result)
        {
            echo '<script language="javascript">alert("添加用户成功!");</script>';
        } else
        {
            echo '<script language="javascript">alert("添加失败!");</script>';
        }
    }
}
?>