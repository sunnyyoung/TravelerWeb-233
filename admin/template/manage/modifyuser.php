<?php
if (!isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">alert("丢!你都无登录!!!");location.href="index.php";</script>';
}
include '../config/DB.php';
include '../config/UPLOAD.php';
$connection = new DB();
$upload = new UPLOAD();
$username = $_GET['username'];
$result = $connection->get_one("SELECT * FROM user WHERE username='" . $username . "'");
if ($result == NULL)
{
    echo '<script>alert("没有此用户!");location.href="home.php?page=alluser&scope=10";</script>';
    exit();
} else
{
    $password = $result['password'];
    $realname = $result['realname'];
    $telephone = $result['telephone'];
}
?>
<div class="content">
    <ul class="breadcrumb">
        <li class="active">修改用户资料</li>
    </ul>
    <div id="main">
        <br/>
        <form method="post" enctype="multipart/form-data">
            <fieldset>
                <p>
                    <label>帐号:</label>
                    <input type="text" readonly="readonly" class="text-medium" value="<?php echo $username; ?>" />
                </p>

                <p>
                    <label>真实姓名</label>
                    <input name="realname" type="text" class="text-long" value="<?php echo $realname; ?>" />
                </p>

                <p>
                    <label>电话号码</label>
                    <input name="telephone" type="tel" class="text-long" value="<?php echo $telephone; ?>" />
                </p>

                <p>
                    <label>新密码</label>
                    <input name="password1" type="password" class="text-long" />
                </p>

                <p>
                    <label>重复新密码</label>
                    <input name="password2" type="password" class="text-long" />
                </p>

                <p>
                    <label>头像</label>
                    <?php
                    if (file_exists('../upload/head/' . $username . '.jpg'))
                    {
                        echo '<img src="../upload/head/' . $username . '.jpg" width="100px" />';
                    } else
                    {
                        echo '<img src="../upload/head/default.jpg" width="100px" />';
                    }
                    ?>
                    <br/>
                    <input name="picfile" type="file" class="text-long" />
                </p>

                <input name="btnChange" type="submit" class="btn btn-primary" id="submit" value="确定" />
            </fieldset>
        </form>
    </div>
</div>
<?php
if (isset($_POST['btnChange']))
{
    $realname = $_POST['realname'];
    $telephone = $_POST['telephone'];
    $password1 = $_POST['password1'];   //新密码
    $password2 = $_POST['password2'];   //重复新密码
    if ($_FILES['picfile']['name'] != '')
    {
        unlink('../upload/head/' . $username . ' . jpg');

        $upload->photp_path = '../upload/head/';
        $upload->get_tmp_name($_FILES['picfile']['tmp_name']);
        $upload->get_type($_FILES['picfile']['type']);
        $upload->get_size($_FILES['picfile']['size']);
        $upload->get_name($username, FALSE);
        $upload->upload_run();
    }

    if ($password1 != $password2)
    {
        //判断两次密码是否一致
        echo '<script language="javascript">alert("两次密码输入不正确,请重试!");</script>';
        return;
    } else
    {
        if ($password1 == '' && $password2 == '')
        {
            $password2 = $password1 = $password;
        } else
        {
            $password1 = md5($password1);
        }
        $userinfo = array(
            'password' => $password1,
            'realname' => $realname,
            'telephone' => $telephone
        );
        //更新数据库密码
        $result = $connection->update('user', $userinfo, "username='" . $username . "'");
        if ($result)
        {
            echo '<script language="javascript">alert("修改成功!");location.href="home.php?page=modifyuser&username=' . $username . '"</script>';
        } else
        {
            echo '<script language="javascript">alert("修改失败!");</script>';
        }
    }
}
?>