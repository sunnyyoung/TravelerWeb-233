<?php

if (!isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">alert("丢!你都无登录!!!");location.href="index.php";</script>';
}
include '../config/DB.php';
$connection = new DB();
switch ($_GET['item'])
{
    case 'user':
        {
            $username = $_GET['username'];
            $result = $connection->get_one("SELECT * FROM user WHERE username='" . $username . "'");
            if ($result == NULL)
            {
                echo '<script>alert("没有此用户!");location.href="home.php?page=alluser&scope=10";</script>';
                exit();
            } else
            {
                unlink('../upload/head/' . $username . '.jpg');
                if ($connection->delete('user', "username='" . $username . "'"))
                {
                    echo '<script>alert("删除用户成功!");location.href="home.php?page=alluser&scope=10";</script>';
                } else
                {
                    echo '<script>alert("删除用户失败!");location.href="home.php?page=alluser&scope=10";</script>';
                    return;
                }
            }
            break;
        }
    case 'post':
        {
            $id = $_GET['id'];
            $result = $connection->get_one("SELECT * FROM data WHERE id=" . $id);
            if ($result == NULL)
            {
                echo '<script>alert("没有此文章!");location.href="home.php?page=alluser&scope=10";</script>';
                exit();
            } else
            {
                unlink('../' . $result['image']);
                if ($connection->delete('data', 'id=' . $id) && $connection->delete('comment', 'id=' . $id))
                {
                    echo '<script>alert("删除文章成功!");location.href="home.php?page=allpost&scope=10";</script>';
                } else
                {
                    echo '<script>alert("删除文章失败!");location.href="home.php?page=allpost&scope=10";</script>';
                    return;
                }
            }
            break;
        }
    case 'comment':
        {
            $time = $_GET['time'];
            $result = $connection->get_one("SELECT * FROM comment WHERE time='" . $time . "'");
            if ($result == NULL)
            {
                echo '<script>alert("没有此评论!");location.href="home.php?page=allcomment&scope=10";</script>';
                exit();
            } else
            {
                if ($connection->delete('comment', "time='" . $time . "'"))
                {
                    echo '<script>alert("删除评论成功!");location.href="home.php?page=allcomment&scope=10";</script>';
                } else
                {
                    echo '<script>alert("删除评论失败!");location.href="home.php?page=allcomment&scope=10";</script>';
                    return;
                }
            }
            break;
        }
    default :
        {
            echo '<script>alert("没有这个项目可以删除");location.href="index.php";</script>';
            break;
        }
}