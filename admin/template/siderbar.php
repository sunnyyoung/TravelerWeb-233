<?php
if (!isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">alert("丢!你都无登录!!!");location.href="index.php";</script>';
}
?>
<div class="sidebar-nav">
    <a class="nav-header">文章管理</a>
    <ul class="nav-list collapse in">
        <li><a href="?page=addpost">新建文章</a></li>
        <li><a href="?page=allpost&scope=10">所有文章</a></li>
    </ul>

    <a class="nav-header">用户管理</a>
    <ul class="nav-list collapse in">
        <li><a href="?page=adduser">新建用户</a></li>
        <li><a href="?page=alluser&scope=10">所有用户</a></li>
    </ul>

    <a class="nav-header">评论管理</a>
    <ul class="nav-list collapse in">
        <li><a href="?page=allcomment&scope=10">所有评论</a></li>
    </ul>

    <a class="nav-header">其他</a>
    <ul class="nav-list collapse in">
        <p><strong>当前时间: </strong><?php echo date('Y-m-d H:i:s', time()); ?></p>
    </ul>
</div>