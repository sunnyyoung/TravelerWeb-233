<?php
if (!isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">alert("丢!你都无登录!!!");location.href="index.php";</script>';
}
?>
<div id="header">
    <div class="navLeft">
        <ul class="clearfix">
            <li>
                <a href="index.php">貮叁叁旅游网 - 后台管理</a>
            </li>
        </ul>
    </div>
    <div class="navRight">
        <ul class="clearfix">
            <li class="menu-user">
                <span class="moreNav1" id="moreNav1"><?php echo $_SESSION['adminusername']; ?></span>
                <div class="navmenu" style="display: none;">
                    <a href="?page=changeinfo">修改资料</a>
                    <a href="?page=logout">退出</a>
                </div>
            </li>
        </ul>
    </div>
</div>