<?php
if (!isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">alert("丢!你都无登录!!!");location.href="index.php";</script>';
}
?>
<div class="content">
    <ul class="breadcrumb">
        <li class="active">修改我的资料</li>
    </ul>
    <div id="main">
        <br/>
        <form method="post">
            <fieldset>
                <p>
                    <label>帐号:</label>
                    <input type="text" readonly="readonly" class="text-medium" value="<?php echo $_SESSION['adminusername']; ?>" />
                </p>

                <p>
                    <label>原始密码</label>
                    <input name="password0" type="password" class="text-long" />
                </p>

                <p>
                    <label>新密码:</label>
                    <input name="password1" type="password" class="text-long" />
                </p>

                <p>
                    <label>重复新密码:</label>
                    <input name="password2" type="password" class="text-long" />
                </p>

                <input name="btnChange" type="submit" class="btn btn-primary" id="submit" value="确定" />
            </fieldset>
        </form>
    </div>
</div>
<?php
include '../config/DB.php';
$connection = new DB();
if (isset($_POST['btnChange']))
{
    $password0 = $_POST['password0'];   //原始密码
    $password1 = $_POST['password1'];   //新密码
    $password2 = $_POST['password2'];   //重复新密码
    if ($password0 == '' || $password1 == '' || $password2 == '')
    {
        //判断资料是否填写完整
        echo '<script language="javascript">alert("信息填写不完整!");</script>';
        return;
    } else if ($password1 != $password2)
    {
        //判断两次密码是否一致
        echo '<script language="javascript">alert("两次密码输入不正确,请重试!");</script>';
        return;
    } elseif (md5($password0) != $_SESSION['adminpassword'])
    {
        //判断原始密码是否正确
        echo '<script language="javascript">alert("原始密码错误,请重试!");</script>';
    } else
    {
        $userinfo = array('password' => md5($password1));
        //更新数据库密码
        $result = $connection->update('admin', $userinfo, "username='" . $_SESSION['adminusername'] . "'");
        if ($result)
        {
            session_destroy();
            echo '<script language="javascript">alert("修改密码成功!请重新登录!");location.href="index.php";</script>';
        } else
        {
            echo '<script language="javascript">alert("修改失败!");</script>';
        }
    }
}
?>