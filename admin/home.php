<?php
session_start();
if (!isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">alert("丢!你都无登录!!!");location.href="index.php";</script>';
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <title>貮叁叁旅游网 - 后台管理</title>
        <link href="css/main.css" rel="stylesheet" type="text/css">
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/custom.js"></script></head>
    <body>
        <?php include 'template/header.php'; ?>
        <?php include 'template/siderbar.php'; ?>
        <?php
        switch ($_GET['page'])
        {
            case 'changeinfo':  //修改密码
                include 'template/changeinfo.php';
                break;
            case 'logout':      //注销
                include 'template/logout.php';
                break;
            case 'addpost':     //新建文章
                include 'template/manage/addpost.php';
                break;
            case 'allpost':     //所有文章
                include 'template/manage/allpost.php';
                break;
            case 'adduser':     //新建用户
                include 'template/manage/adduser.php';
                break;
            case 'alluser':     //所有用户
                include 'template/manage/alluser.php';
                break;
            case 'modifyuser':
                include 'template/manage/modifyuser.php';
                break;
            case 'modifypost':
                include 'template/manage/modifypost.php';
                break;
            case 'allcomment':
                include 'template/manage/allcomment.php';
                break;
            case 'delete':
                include 'template/manage/delete.php';
                break;
            default:            //欢迎页面
                include 'template/welcome.php';
                break;
        }
        ?>
    </body>
</html>