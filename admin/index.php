<?php
session_start();
if (isset($_SESSION['adminusername'], $_SESSION['adminpassword']))
{
    echo '<script language="javascript">location.href="home.php";</script>';
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <title>貮叁叁旅游网 - 后台登录</title>
        <!-- login css -->
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="loginform">
            <form method="post">
                <div class="fieldContainer">
                    <h1 id="h1">貮叁叁旅游网后台</h1>
                    <div class="formRow">
                        <div class="field">
                            <input type="text" placeholder="用户名" name="username" />
                        </div>
                    </div>
                    <div class="formRow">
                        <div class="field">
                            <input type="password" placeholder="密码" name="password" />
                        </div>
                    </div>
                    <div class="formRow">
                        <div class="field">
                            <input type="text" placeholder="验证码" name="validcode" />
                            <img src="../config/validcode.php" style="float: right;"/>
                        </div>
                    </div>
                </div>
                <div class="signupButton">
                    <button type="submit" name="btnLogin" id="submit" value="登录" />
                </div>
            </form>
        </div>
        <?php
        //连接数据库初始化
        include '../config/DB.php';
        $connection = new DB();
        if (isset($_POST['btnLogin']))
        {
            $adminusername = $_POST['username'];    //登录用户名
            $adminpassword = $_POST['password'];    //登录密码
            $adminvalidcode = $_POST['validcode'];  //验证码
            if ($adminpassword == '' || $adminpassword == '')
            {
                //判断是否为空
                echo '<script language="javascript">alert("用户名或密码不能为空!");</script>';
                return;
            } elseif (md5($adminvalidcode) != $_SESSION['validcode'])
            {
                //判断验证码对错
                echo '<script language="javascript">alert("验证码错误,请重试!");</script>';
                return;
            }

            $result = $connection->query("SELECT * FROM admin WHERE username='"
                    . $adminusername
                    . "'AND password='"
                    . md5($adminpassword) . "'");
            if ($connection->num_rows($result) > 0)
            {
                $_SESSION['adminusername'] = $adminusername;
                $_SESSION['adminpassword'] = md5($adminpassword);
                echo '<script language="javascript">alert("登陆成功");location.href="home.php";</script>';
            } else
            {
                echo '<script language="javascript">alert("登陆失败,请检查用户名或密码是否正确");</script>';
            }
        }
        ?>
    </body>
</html>