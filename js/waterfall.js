$(document).ready(function()
{
    loadMore();
});

$(window).scroll(function()
{
    // 当滚动到最底部以上100像素时， 加载新内容
    if ($(document).height() - $(this).scrollTop() - $(this).height() < 100 )
        loadMore();
});


function loadMore()
{
    $.ajax({
        url: 'config/data.php',
        dataType: 'json',
        success: function(json)
        {
            if (typeof json === 'object')
            {
                var oProduct, $row, iHeight, iTempHeight;
                for (var i = 0, l = json.length; i < l; i++)
                {
                    oProduct = json[i];

                    iHeight = -1;
                    $('#stage li').each(function() {
                        iTempHeight = Number($(this).height());
                        if (iHeight === -1 || iHeight > iTempHeight)
                        {
                            iHeight = iTempHeight;
                            $row = $(this);
                        }
                    });

                    $item = $('<div><a href="?page=showpost&id=' + oProduct.id + '"><img src="' + oProduct.image + '" border="0" width="200" style="border-radius: 6px;" ></a><span>' + oProduct.introduction + '</span></div>').hide();

                    $row.append($item);
                    $item.fadeIn();
                }
            }
        }
    });
}