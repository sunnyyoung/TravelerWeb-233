<?php session_start(); ?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel='stylesheet' href='css/main.css' />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.popmenu.js"></script>
        <script type="text/javascript" src="js/menu.js"></script>
        <script type="text/javascript" src="js/scrolltopcontrol.js"></script>
        <title>233旅游网</title>
    </head>
    <body>
        <!--导航栏开始-->
        <?php include 'template/header.php'; ?>
        <!--导航栏结束-->

        <?php
        switch ($_GET['page'])
        {
            case '':
                include 'template/home.php';
                break;
            case 'register':
                include 'template/register.php';
                break;
            case 'login':
                include 'template/login.php';
                break;
            case 'logout':
                session_destroy();
                include 'template/logout.php';
                break;
            case 'changepsw':
                include 'template/changepsw.php';
                break;
            case 'changeinfo':
                include 'template/changeinfo.php';
                break;
            case 'showpost':
                include 'template/showpost.php';
                break;
            case 'search':
                include 'template/search.php';
                break;
            default :
                include 'template/404.php';
                break;
        }
        ?>
        
        <!--底部栏开始-->
        <?php include 'template/footer.php'; ?>
        <!--底部栏结束-->
    </body>
</html>